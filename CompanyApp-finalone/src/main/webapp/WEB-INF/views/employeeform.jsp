<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.styled-table {
	border-radius: 20px;
	border-collapse: collapse;
	margin: 25px 0;
	font-size: 0.9em;
	font-family: sans-serif;
	min-width: 400px;
	box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}

.styled-table thead tr {
	background-color: #009879;
	color: #ffffff;
	text-align: left;
}

.styled-table th, .styled-table td {
	padding: 12px 15px;
}

.styled-table tbody tr {
	border-bottom: 1px solid #dddddd;
}

.styled-table tbody tr:nth-of-type(even) {
	background-color: #f3f3f3;
}
/*
.styled-table tbody tr:last-of-type {
    border-bottom: 2px solid #009879;
} */
.styled-table tbody tr.active-row {
	font-weight: bold;
	color: #009879;
}

.rounded {
	border-radius: 5px;
	color: MidnightBlue;
	padding: 4px 20px;
	border-width: thin;
}
/*
a{
font-weight: bold;
font-size: large;}*/
body {
	background-color: Lightyellow;
}

a {
	background-color: gray; /* Blue */
	border-radius: 10px;
	color: white;
	padding: 2px 4px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: medium;
}
</style>
</head>
<body>

	<div align="center">
		<center>
			<h1 style="font-family: monospace; color: MidnightBlue">New/Edit
				Employee</h1>
		</center>

		<form:form action="saveData" method="post" modelAttribute="employee">
			<table class="styled-table">
				<form:hidden path="employeeId" />
				<tr>
					<td>Name:</td>
					<td><form:input path="name" class="rounded" /></td>
				</tr>
				<tr>
					<td>Project Details:</td>
					<td><form:input path="project" class="rounded" /></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><form:input path="mailId" class="rounded" /></td>
				</tr>
				<tr>
					<td>Phone Number:</td>
					<td><form:input path="phoneNo" class="rounded" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" value="Save"
						style="border-radius: 10px; color: MidnightBlue; padding: 4px 20px;"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>