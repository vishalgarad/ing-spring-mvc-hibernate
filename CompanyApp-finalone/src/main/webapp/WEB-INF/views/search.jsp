<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search</title>
<style type="text/css">
.styled-table {
    border-collapse: collapse;
    margin: 25px 0;
    font-size: 0.9em;
    font-family: sans-serif;
    min-width: 400px;
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}
.styled-table thead tr {
    background-color: #DAA520;
    color: #ffffff;
    text-align: left;
}
.styled-table th,
.styled-table td {
    padding: 12px 15px;
}
.styled-table tbody tr {
    border-bottom: 1px solid #dddddd;
}

.styled-table tbody tr:nth-of-type(even) {
    background-color: #f3f3f3;
}

.styled-table tbody tr:last-of-type {
    border-bottom: 2px solid #DAA520;
}
.styled-table tbody tr.active-row {
    font-weight: bold;
    color: #DAA520;
}
/*
a{
font-weight: bold;
font-size: large;}*/
body{
background-color:#F0FFF0;} 
a {
background-color:gray; /* Blue */
border-radius: 10px;
color: white;
padding: 4px 15px;
text-align: center;
text-decoration: none;
display: inline-block;
font-size: medium;
}

.rounded{
border-radius: 5px;
color:MidnightBlue;
padding: 4px 20px;
border-width: thin;
}
</style>
</head>
<body>
		<a  href="emplist" class="w3-button w3-round-xxlarge">All Employee</a>&nbsp;
		<a href="addemployee" class="w3-button w3-round-xxlarge">Add Employee</a>

	<div align="center">
		<center><h1 style="font-family:monospace;color:MidnightBlue">Search</h1></center>
		<form:form  action="search" method="post">
		<input type="text" name="employeeName"  class="rounded">
			<button type="submit" name="search" class="rounded">Search</button>
		</form:form>
	</div>
	
	<br/>
	<br/>
	
	
			
	<div align="center">
		<table  class="styled-table">
		<thead>
		   <tr>
			<th style="border-radius: 10px 0px 0px 0px;">Name</th>
			<th>ProjectDetails</th>
			<th>Mail Id</th>
			<th>phone number</th>
			<th></th>
			<th style="border-radius: 0px 10px 0px 0px;"></th>
           </tr>
           </thead>
           <tbody>
			<c:forEach items="${emplist}" var="emp">
				<tr align ="center" class="active-row">
					<td>${emp.name}</td>
					<td>${emp.project}</td>
					<td>${emp.mailId}</td>
					<td>${emp.phoneNo}</td>
					<td><a href="updateEmployee?employeeId=${emp.employeeId}">Edit</a>
					<td><a href="deleteEmployee/${emp.employeeId}">Delete</a>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>