<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.styled-table {
	border-radius: 20px;
    border-collapse: collapse;
    margin: 25px 0;
    font-size: 0.9em;
    font-family: sans-serif;
    min-width: 400px;
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
    
}
.styled-table thead tr {
    background-color: #1E90FF;
    color: #ffffff;
    text-align: left;
    
}
.styled-table th,
.styled-table td {
    padding: 12px 15px;

}
.styled-table tbody tr {
    border-bottom: 1px solid #dddddd;
}

.styled-table tbody tr:nth-of-type(even) {
    background-color: #f3f3f3;
}

.styled-table tbody tr:last-of-type {
    border-bottom: 2px solid #1E90FF;
}
.styled-table tbody tr.active-row {
    font-weight: bold;
    color: #1E90FF;
}
/*
a{
font-weight: bold;
font-size: large;}
body{
background-color:LightGray;} */
a {
background-color:gray; /* Blue */
border-radius: 10px;
color: white;
padding: 4px 15px;
text-align: center;
text-decoration: none;
display: inline-block;
font-size: medium;
}
</style>
</head>
<body>
<br>
<br>
		<a  href="search" class="w3-button w3-round-xxlarge">Search Employee</a>&nbsp;
		<a href="addemployee" class="w3-button w3-round-xxlarge">Add Employee</a>
		
	<center><h1 style="font-family:monospace;color:MidnightBlue">Employee List</h1></center>
 
	<div align="center">
		<table class="styled-table">
		<thead>
		   <tr>
			<th style="border-radius: 10px 0px 0px 0px;">Name</th>
			<th>ProjectDetails</th>
			<th>Mail ID</th>
			<th>Phone Number</th>
			<th></th>
			<th style="border-radius: 0px 10px 0px 0px;"></th>
           </tr>
           </thead>
           <tbody>
			<c:forEach items="${employeeList}" var="emp">
				<tr class="active-row">
					<td>${emp.name}</td>
					<td>${emp.project}</td>
					<td>${emp.mailId}</td>
					<td>${emp.phoneNo}</td>
					<td><a href="updateEmployee?employeeId=${emp.employeeId}">Edit</a>
					<td><a href="deleteEmployee/${emp.employeeId}">Delete</a>
				</tr>
			</c:forEach>
			<tbody>
		</table>
		
	</div>
</body>
</html>